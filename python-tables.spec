%global with_python3 0%{?fedora} || 0%{?rhel} >= 7

%{?filter_setup:
%filter_provides_in %{python_sitearch}/.*\.so$
%filter_provides_in %{python3_sitearch}/.*\.so$
%filter_provides_in %{python3_other_sitearch}/.*\.so$
%filter_setup}

%global module  tables

#global commit 16191801a53eddae8ca9380a28988c3b5b263c5e
#global shortcommit %(c=%{commit}; echo ${c:0:7})

# Use the same directory of the main package for subpackage licence and docs
%global _docdir_fmt %{name}

Summary:        Hierarchical datasets in Python
Name:           python3-%{module}
Version:        3.2.0
Release:        2.4%{?dist}%{?gitcommit:.git%{shortcommit}}
#Source0:        https://github.com/PyTables/PyTables/archive/%{commit}/PyTables-%{commit}.tar.gz
Source0:        https://github.com/PyTables/PyTables/archive/v.%{version}.tar.gz

Source1:        https://sourceforge.net/projects/pytables/files/pytables/%{version}/pytablesmanual-%{version}.pdf
Patch0:         always-use-blosc.diff
Patch1:         hdf5-blosc-1.4.4-1.6.1.diff

License:        BSD
Group:          Development/Languages
URL:            http://www.pytables.org
Requires:       numpy
Requires:       python-numexpr

BuildRequires:  hdf5-devel >= 1.8 bzip2-devel lzo-devel
BuildRequires:  Cython >= 0.13 numpy python-numexpr
BuildRequires:  blosc-devel >= 1.5.2

%description
PyTables is a package for managing hierarchical datasets and designed
to efficiently and easily cope with extremely large amounts of data.

%package -n python%{python3_pkgversion}-%{module}
%{?python_provide:%python_provide python%{python3_pkgversion}-%{module}}
Summary:        Hierarchical datasets in Python
BuildRequires:  python%{python3_pkgversion}-Cython >= 0.13 python%{python3_pkgversion}-numpy python%{python3_pkgversion}-numexpr >= 2.2
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools

Requires:       python%{python3_pkgversion}-numpy
Requires:       python%{python3_pkgversion}-numexpr

%description -n python%{python3_pkgversion}-%{module}
PyTables is a package for managing hierarchical datasets and designed
to efficiently and easily cope with extremely large amounts of data.

This is the version for Python 3.

%package        doc
Group:          Development/Languages
Summary:        Documentation for PyTables
BuildArch:      noarch

%description doc
The %{name}-doc package contains the documentation for %{name}.

%prep
%autosetup -n PyTables-v.%{version} -p1

echo "import sys, tables; sys.exit(tables.test(verbose=1))" > bench/check_all.py
# Make sure we are not using anything from the bundled blosc by mistake
find c-blosc -mindepth 1 -maxdepth 1 -name hdf5 -prune -o -exec rm -r {} +

cp -a %{SOURCE1} pytablesmanual.pdf

%build
python3 setup.py build

%check
libdir=`ls build/|grep lib`
export PYTHONPATH=`pwd`/build/$libdir LANG=en_US.UTF-8
#python bench/check_all.py

# OOM during tests on s390
%ifnarch s390
libdir=`ls build/|grep lib`
export PYTHONPATH=`pwd`/build/$libdir
#python3 bench/check_all.py
%endif

%install
chmod -x examples/check_examples.sh
for i in utils/*; do sed -i 's|bin/env |bin/|' $i; done

python3 setup.py install -O1 --skip-build --root=%{buildroot}

for exe in ptdump ptrepack pt2to3 pttree ; do
    mv $RPM_BUILD_ROOT%{_bindir}/${exe} $RPM_BUILD_ROOT%{_bindir}/${exe}-py36
done

#%files
#%license LICENSE.txt LICENSES
#%{python_sitearch}/%{module}
#%{python_sitearch}/%{module}-%{version}*.egg-info

%files -n python%{python3_pkgversion}-%{module}
%license LICENSE.txt LICENSES
%{python3_sitearch}/%{module}
%{python3_sitearch}/%{module}-%{version}*.egg-info
%{_bindir}/ptdump-py36
%{_bindir}/ptrepack-py36
%{_bindir}/pt2to3-py36
%{_bindir}/pttree-py36

%files doc
%license LICENSE.txt LICENSES
%doc pytablesmanual.pdf
%doc [A-KM-Za-z]*.txt
%doc examples/

%changelog
* Wed Nov 20 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 3.2.0-2.4
- Rename src rpm

* Thu Nov 14 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 3.2.0-2.3
- Rename binaries in /usr/bin

* Thu Oct 3 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 3.2.0-2.2
- Drop python3.4, python2 support

* Thu Nov 29 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 3.2.0-2.1
- Add python3.6 support

* Fri Nov  9 2018 Zbigniew Michael Thomas <michael.thomas@LIGO.ORG> - 3.2.0-2
- Update Provides

* Thu May  7 2015 Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl> - 3.2.0-1
- Update to 3.2.0

* Thu Jan  8 2015 Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl> - 3.1.2-4.git1619180
- Use blosc on all architectures

* Thu Jan  8 2015 Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl> - 3.1.2-3.git1619180
- Update to latest snapshot and use external blosc

* Wed Jan 07 2015 Orion Poplawski <orion@cora.nwra.com> - 3.1.1-2
- Rebuild for hdf5 1.8.14

* Tue Jan 06 2015 Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl> - 3.1.1-1
- Update to 3.1.1 (#1080889)

* Tue Nov 25 2014 Dan Horák <dan[at]danny.cz> - 3.0.0-8
- workaround OOM during Python3 tests on s390

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.0.0-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.0.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Wed May 28 2014 Kalev Lember <kalevlember@gmail.com> - 3.0.0-5
- Rebuilt for https://fedoraproject.org/wiki/Changes/Python_3.4

* Mon Mar 24 2014 Zbigniew Jędrzejewski-Szmek - 3.0.0-4
- Rebuild for latest blosc

* Fri Jan 10 2014 Zbigniew Jędrzejewski-Szmek - 3.0.0-3
- Move python3 requires to the proper package (#1051691)

* Thu Sep 05 2013 Zbigniew Jędrzejewski-Szmek - 3.0.0-2
- Add python3-tables package

* Wed Aug 21 2013 Thibault North <tnorth@fedoraproject.org> - 3.0.0-1
- Update to 3.0.0

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.4.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu May 16 2013 Orion Poplawski <orion@cora.nwra.com> - 2.4.0-3
- Rebuild for hdf5 1.8.11

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.4.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Sun Oct 28 2012 Thibault North <tnorth@fedoraproject.org> - 2.4.0-1
- Update to 2.4.0

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.3.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.3.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Mon Nov 14 2011 Thibault North <tnorth@fedoraproject.org> - 2.3.1-3
- Remove lrucache.py which was deprecated and under AFL license

* Thu Nov 10 2011 Thibault North <tnorth@fedoraproject.org> - 2.3.1-2
- Fixes and subpackage for the docs

* Mon Nov 07 2011 Thibault North <tnorth@fedoraproject.org> - 2.3.1-1
- Fixes and update to 2.3.1
